# Sakuradite

Sukuradite is a command-line utility that is designed to centralize development tools in one place.

## Installation

```bash
npm install --global sakuradite
```

## Usage
```
skd <command>

Commands:
  skd create <name>  create project
  skd build          build project
  skd clean          clean project
  skd run            run project
  skd service        run service
  skd test           run tests
  skd doctor         run and show diagnostic information

Options:
  --help         Show help                                             [boolean]
  --version      Show version number                                   [boolean]
  --verbose, -v  causes more info to be shown                            [count]
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
