const { join } = require('path');

const SAKURA_PATH = join(__dirname, '..');
const SRC_PATH = join(SAKURA_PATH, 'src');
const PROJECT_PATH = join(SRC_PATH, 'projects');

module.exports = {
  // webpack modes
  DEVELOPMENT: 'development',
  TEST: 'test',
  INTEGRATION_TEST: 'integration-test',
  PRODUCTION: 'production',

  SAKURA_PATH,
  SRC_PATH,
  PROJECT_PATH,

  NODE_PATH: join(SAKURA_PATH, 'node_modules'),
  WEBPACK_CONFIG_PATH: join(SRC_PATH, 'projects', 'common', 'webpack.config.js'),
};
