const { DEVELOPMENT, PRODUCTION, NODE_PATH, WEBPACK_CONFIG_PATH } = require('./constants');
const { getSakuraditeType } = require('./package-json');
const { execBind, moduleBinPath } = require('./utils');

const runWebpack = (nodeEnv = PRODUCTION, watch = false, afterBuild = null, options = {}) => {
  return getSakuraditeType()
    .then(SAKURADITE_PROJECT_TYPE => {
      if(nodeEnv === PRODUCTION && watch)
        nodeEnv = DEVELOPMENT;

      const env = {
        ...process.env,
        NODE_ENV: nodeEnv,
        NODE_PATH,
        SAKURADITE_PROJECT_TYPE,
      };

      if(options.verbose)
        env.SAKURADITE_VERBOSE = options.verbose;

      if(afterBuild)
        env.AFTER_BUILD = afterBuild;

      const commandPath = moduleBinPath('webpack-cli');
      const parts = [commandPath, '--color', '--config', WEBPACK_CONFIG_PATH];
      if(watch)
        parts.push('--watch');

      const command = parts.join(' ');

      console.log(`== Running Webpack: ${command} ==`);
      return execBind(command, { env });
    })
    .catch(e => console.error('Failure in webapp:', e));
};

module.exports = { runWebpack };
