const { access, accessSync } = require('fs');
const childProcess = require('child_process');
const { join } = require('path');
const { promisify } = require('util');

const { spawn } = childProcess;

const exec = promisify(childProcess.exec);

const simplifyNodeModulesPath = path => {
  if(/\blib\b/.test(path))
    return path;

  const parts = path.split('node_modules');
  return `${parts[0]}node_modules${parts[parts.length - 1]}`;
};

const bind = fn => (...args) => {
  let childProcess = null;
  const promise = new Promise((resolve, reject) => {
    childProcess = fn(...args);

    childProcess.stdout.pipe(process.stdout);
    childProcess.stderr.pipe(process.stderr);

    // eslint-disable-next-line prefer-promise-reject-errors
    childProcess.on('error', (...args) => reject(...args));
    childProcess.on('close', (...args) => resolve(...args));
  });

  promise.getChildProcess = () => childProcess;

  return promise;
};

const execBind = bind(childProcess.exec);
const spawnBind = bind(spawn);

const fileExists = path => {
  return new Promise(resolve => {
    access(path, err => err ? resolve(false) : resolve(true));
  });
};

const fileExistsSync = path => {
  try {
    accessSync(path);
  } catch(e) {
    return false;
  }

  return true;
};

const moduleBinPath = (mod, bin = null) => {
  if(bin === null)
    bin = mod;

  for(const path of module.paths) {
    const binPath = join(path, '.bin', bin);
    const exists = fileExistsSync(binPath);

    if(exists)
      return binPath;
  }
};

module.exports = { exec, execBind, fileExists, moduleBinPath, simplifyNodeModulesPath, spawnBind };
