const { join } = require('path');

const { INTEGRATION_TEST, TEST } = require('../constants');
const { execBind, moduleBinPath } = require('../utils');
const { runWebpack } = require('../webpack');

module.exports = async({ int, watch }) => {
  const [testType, specFile] = int ? [INTEGRATION_TEST, 'spec.int'] : [TEST, 'spec'];

  const mochaPath = moduleBinPath('mocha');
  const specPath = join('dist', specFile);

  const mochaCmd = `${mochaPath} --color ${specPath} --require source-map-support/register`;
  if(watch)
    await runWebpack(testType, true, mochaCmd);
  else {
    const webpackStatus = await runWebpack(testType);
    if(webpackStatus !== 0)
      process.exit(webpackStatus);

    const mochaStatus = await execBind(mochaCmd)
      .catch(e => console.log('Failed to run mocha:', e));
    if(mochaStatus !== 0)
      process.exit(mochaStatus);
  }
};
