const rimraf = require('rimraf');

module.exports = ({ verbose }) => {
  if(verbose)
    console.log('Removing `./dist` directory');

  rimraf.sync('dist');
};
