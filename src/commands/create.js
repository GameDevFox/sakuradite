const { mkdirSync, promises: { rename } } = require('fs');
const { join } = require('path');
const copy = require('recursive-copy');

const { PROJECT_PATH } = require('../constants');
const { updatePackageJson } = require('../package-json');
const { exec, fileExists } = require('../utils');

const runProjectHooks = args => {
  return new Promise(resolve => {
    const projectHookPath = join(PROJECT_PATH, args.type, 'index.js');
    fileExists(projectHookPath).then(exists => {
      if(!exists)
        return;

      const projectHooks = require(projectHookPath);
      if(projectHooks.onCreate)
        resolve(projectHooks.onCreate(args));
      else
        resolve();
    });
  });
};

module.exports = args => {
  const { name, type } = args;

  const createProject = () => {
    console.log();
    console.log(`Creating a \`${type}\` project named \`${name}\``);

    mkdirSync(name);

    const copyOptions = { dot: true };

    const promises = [
      exec('git init', { cwd: name }),
      exec('npm init --yes', { cwd: name })
        .then(() => updatePackageJson(name, type)),
      copy(join(PROJECT_PATH, 'common', 'files'), name, copyOptions),
      copy(join(PROJECT_PATH, type, 'files'), name, copyOptions),
    ];

    return Promise.all(promises)
      .then(() => rename(join(name, 'gitignore'), join(name, '.gitignore')))
      .then(() => runProjectHooks(args))
      .then(() => exec('git add .', { cwd: name }))
      .then(() => {
        console.log();
        console.log('Project created');
      })
      .catch(error => {
        console.error('Failed to create project...', error);
      });
  };

  fileExists(name).then(exists => {
    if(exists) {
      console.error(`Error: There is already a file in this directory named \`${name}\``);
      return;
    }

    createProject();
  });
};
