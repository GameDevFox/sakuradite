const { spawnBind } = require('../utils');
const { runWebpack } = require('../webpack');

module.exports = async() => {
  const exitStatus = await runWebpack();
  if(exitStatus !== 0)
    process.exit(exitStatus);

  console.log();
  console.log('== BUILD SUCCESS ==');
  console.log();

  await spawnBind('node', ['./dist/main']);
};
