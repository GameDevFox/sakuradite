const express = require('express');
const { spawn } = require('child_process');

const { DEVELOPMENT, NODE_PATH, WEBPACK_CONFIG_PATH } = require('../constants');
const { getSakuraditeType } = require('../package-json');
const { execBind, moduleBinPath } = require('../utils');
const { runWebpack } = require('../webpack');

const serveApp = () => {
  let proc = null;

  const app = express();

  app.get('/run', (req, res) => {
    if(proc)
      proc.kill('sigterm');

    proc = spawn('node', ['--require', `${NODE_PATH}/source-map-support/register`, './dist/main']);
    proc.stdout.pipe(process.stdout);
    proc.stderr.pipe(process.stderr);

    res.sendStatus(200);
  });

  const listener = app.listen(() => {
    const { port } = listener.address();
    console.log('Listening', port);

    runWebpack(DEVELOPMENT, true, `curl --output /dev/null --silent http://localhost:${port}/run`);
  });
};

const binPath = moduleBinPath('webpack-dev-server');
const webpackDevServerCmd = `${binPath} --color --hot --config ${WEBPACK_CONFIG_PATH}`;
const runWebpackDevServer = ({ verbose }) => {
  const env = {
    ...process.env,
    NODE_ENV: DEVELOPMENT,
    SAKURADITE_PROJECT_TYPE: 'webapp',
  };

  if(verbose)
    env.SAKURADITE_VERBOSE = verbose;

  const promise = execBind(webpackDevServerCmd, { env });
  promise.catch(e => console.error('Failed to serve...', e))

  if(verbose) {
    const { pid } = promise.getChildProcess();
    console.log(`\`webpack-dev-server\` PID: ${pid}`);
  }

  return promise;
};

const services = {
  app: serveApp,
  webapp: runWebpackDevServer,
};

module.exports = (...args) => {
  return getSakuraditeType()
    .then(type => {
      const service = services[type];
      if(!service) {
        console.error(`Cannot create service for project type: ${type}`);
        return;
      }

      service(...args);
    })
    .catch(e => console.error('Failed to serve...', e));
};
