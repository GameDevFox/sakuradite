const { PRODUCTION } = require('../constants');
const { runWebpack } = require('../webpack');

module.exports = ({ watch, verbose }) => runWebpack(PRODUCTION, watch, null, { verbose });
