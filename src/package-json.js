#!/usr/bin/env node
const { join } = require('path');
const { promises: { readFile, writeFile } } = require('fs');

const readJsonFile = path => {
  return readFile(path, 'utf8')
    .then(json => JSON.parse(json));
};

const writeJsonFile = (path, jsonObj) => {
  const json = JSON.stringify(jsonObj, null, 2) + '\n';
  return writeFile(path, json, 'utf8');
};

const getSakuraditeType = (path = '.') => {
  const packageJsonPath = join(path, 'package.json');
  return readJsonFile(packageJsonPath)
    .then(({ sakuradite }) => {
      if(!(sakuradite && sakuradite.type))
        throw new Error('Could not find `sakuraite.type` in `package.json`');

      return sakuradite.type;
    })
    .catch(() => null);
};

const updatePackageJson = (path, type) => {
  return readJsonFile(join(path, 'package.json'))
    .then(jsonObj => {
      jsonObj.main = 'dist/main.js';
      jsonObj.sakuradite = { type };

      return writeJsonFile(join(path, 'package.json'), jsonObj);
    });
};

module.exports = { readJsonFile, writeJsonFile, getSakuraditeType, updatePackageJson };
