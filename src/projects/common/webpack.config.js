'use strict';

const { join } = require('path');
const findFilesSync = require('recursive-readdir-sync');

const {
  DEVELOPMENT, TEST, INTEGRATION_TEST, PRODUCTION,
  NODE_PATH, SRC_PATH, PROJECT_PATH,
} = require('../../constants');
const { execBind } = require('../../utils');

const buildConfig = () => {
  const {
    AFTER_BUILD,
    NODE_ENV,
    SAKURADITE_PROJECT_TYPE,
    SAKURADITE_VERBOSE,
  } = process.env;

  if(!SAKURADITE_PROJECT_TYPE)
    throw new Error('SAKURADITE_PROJECT_TYPE environment variable must be set');

  const eslintConfigFile = join(PROJECT_PATH, SAKURADITE_PROJECT_TYPE, '.eslintrc.yml');
  const webpackEnv = NODE_ENV || PRODUCTION;

  if(SAKURADITE_VERBOSE) {
    console.log();
    console.log('ESLint Config File:', eslintConfigFile);
    console.log('Sakura Node Path:', NODE_PATH);
    console.log('Webpack ENV:', webpackEnv);
    console.log();
  }

  const AfterEmitPlugin = fn => ({
    apply: compiler => compiler.hooks.done.tap('AfterEmitPlugin', fn),
  });

  // default production config
  let config = {
    mode: PRODUCTION,

    module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        enforce: 'pre',
        options: {
          configFile: eslintConfigFile,
          fix: true,
        },
      }, {
        test: /\.raw$/,
        loader: 'raw-loader',
      }],
    },

    resolve: {
      modules: ['src', 'node_modules', NODE_PATH],
    },

    resolveLoader: {
      modules: [NODE_PATH],
    },
  };

  // Project type specific config
  const webpackConfPath = join('..', SAKURADITE_PROJECT_TYPE, 'webpack.config');
  const extend = require(webpackConfPath);
  config = extend({ config, webpackEnv });

  if(webpackEnv === DEVELOPMENT) {
    Object.assign(config, {
      mode: DEVELOPMENT,
      devtool: 'source-map',
    });
  }

  if(webpackEnv === TEST) {
    const testFiles = findFilesSync('./src')
      .filter(file => /\.spec.js$/.test(file))
      .map(file => `./${file}`);

    Object.assign(config, {
      mode: DEVELOPMENT,
      entry: [join(SRC_PATH, 'index.spec.js'), ...testFiles],
      output: {
        filename: 'spec.js',
      },
      devtool: 'source-map',
    });
  }

  if(webpackEnv === INTEGRATION_TEST) {
    const testFiles = findFilesSync('./src')
      .filter(file => /\.spec.int.js$/.test(file))
      .map(file => `./${file}`);

    Object.assign(config, {
      mode: DEVELOPMENT,
      entry: [join(SRC_PATH, 'index.spec.js'), ...testFiles],
      output: {
        filename: 'spec.int.js',
      },
      devtool: 'source-map',
    });
  }

  if(AFTER_BUILD) {
    config.plugins = [AfterEmitPlugin(() => {
      setTimeout(() => {
        console.log();
        console.log(`== Running \`${AFTER_BUILD}\` ==`);
        execBind(AFTER_BUILD).catch(e => console.error('Failed  to run `AFTER_BUILD`:', e));
      }, 0);
    })];
  }

  if(SAKURADITE_VERBOSE) {
    console.log('Webpack Config:');
    console.log(JSON.stringify(config, null, 2));
  }

  return config;
};

module.exports = new Promise((resolve, reject) => {
  try {
    const config = buildConfig();
    resolve(config);
  } catch(e) {
    reject(new Error('Webpack Config Failure:', e));
  }
});
