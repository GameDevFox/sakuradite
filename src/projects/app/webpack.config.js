const nodeExternals = require('webpack-node-externals');

module.exports = ({ config }) => {
  const output = config.output || {};
  output.libraryTarget = 'umd';
  config.output = output;

  config.target = 'node';
  config.externals = [nodeExternals()];

  config.devtool = 'source-map';

  return config;
};
