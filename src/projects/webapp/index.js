const { execBind } = require('../../utils');

const onCreate = ({ name }) => {
  const packages = [
    '@babel/core',
    '@babel/plugin-proposal-export-default-from',
    '@babel/preset-react',
    '@hot-loader/react-dom',
    'react',
    'react-dom',
    'react-hot-loader',
    'react-redux',
    'redux',
    'semantic-ui-react',
    'styled-components',
  ];

  const npmCommand = `npm install --save-dev ${packages.join(' ')}`;
  console.log('NPM Command:', npmCommand);
  return execBind(npmCommand, { cwd: name })
    .catch(e => console.error('npm install failed...', e));
};

module.exports = { onCreate };
