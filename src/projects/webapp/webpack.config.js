const HtmlWebpackPlugin = require('html-webpack-plugin');
const { join } = require('path');
const process = require('process');

const { DEVELOPMENT } = require('../../constants');

module.exports = ({ config, webpackEnv }) => {
  config.output = {
    filename: '[name]-[hash].js',
  };

  config.module.rules.push({
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-react'],
        plugins: [
          '@babel/plugin-proposal-export-default-from',
          '@babel/plugin-proposal-optional-chaining',
          'react-hot-loader/babel',
        ],
      },
    },
  });

  config.module.rules.push({
    test: /\.css$/,
    use: [
      { loader: 'style-loader' },
      { loader: 'css-loader' },
    ],
  });

  config.plugins = [
    new HtmlWebpackPlugin({ template: './src/index.html' }),
  ];

  config.resolve = {
    ...config.resolve,

    alias: {
      Root: join(process.cwd(), 'src'),
      App: join(process.cwd(), 'src', 'app'),
      Env: join(process.cwd(), 'src', 'env', webpackEnv),

      react: join(process.cwd(), 'node_modules', 'react'),
      'react-dom': '@hot-loader/react-dom',
    },
  };

  if(webpackEnv === DEVELOPMENT) {
    config.devServer = {
      historyApiFallback: true,
      host: '0.0.0.0',
      hot: true,
      port: 3000,
      overlay: true,
    };
  }

  return config;
};
