import createStore from 'Env/create-store';

import { Reducer } from './redux-utils';

const reducer = Reducer({
  MY_ACTION: (state, action) => {
    const { increment } = action;
    return { ...state, count: state.count + increment };
  },
}, {
  name: 'Some App',
  count: 123,
});

const store = createStore(reducer);
export default store;
