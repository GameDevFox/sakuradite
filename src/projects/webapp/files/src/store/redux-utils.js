/* eslint-disable */
import _ from 'lodash';
import { combineReducers, createStore } from 'redux';

const Action = type => {
  return args => ({ ...args, type });
};

const Reducer = (handlers, initialState) => {
  return (oldState, action) => {
    if(oldState === undefined)
      return initialState;

    const { type } = action;
    const actionHandler = handlers[type];

    let state = oldState;
    if(actionHandler)
      state = actionHandler(oldState, action);

    return state;
  };
};

export { Action, Reducer };
