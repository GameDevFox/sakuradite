import React from 'react';
import { render } from 'react-dom';

import App from './app';

// Render App
render(<App/>, document.getElementById('root'));
