import React from 'react';
import { hot } from 'react-hot-loader/root';
import { Provider } from 'react-redux';

import store from './store';

const App = () => (
  <Provider store={store}>
    Hello World
  </Provider>
);

export default hot(App);
